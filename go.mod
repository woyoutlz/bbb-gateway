module bitbucket.org/woyoutlz/bbb-gateway

require (
	coding.net/yundkyy/cybexgolib v0.8.10
	github.com/btcsuite/btcd v0.0.0-20190418232430-6867ff32788a
	github.com/denkhaus/logging v0.0.0-20180714213349-14bfb935047c // indirect
	github.com/emirpasic/gods v1.12.0 // indirect
	github.com/gin-contrib/sse v0.0.0-20190301062529-5545eab6dad3 // indirect
	github.com/gin-gonic/gin v1.3.0
	github.com/jinzhu/gorm v1.9.4
	github.com/juju/errors v0.0.0-20190207033735-e65537c515d7 // indirect
	github.com/mattn/go-isatty v0.0.7 // indirect
	github.com/mitchellh/mapstructure v1.1.2
	github.com/petermattis/goid v0.0.0-20180202154549-b0b1615b78e5 // indirect
	github.com/pquerna/ffjson v0.0.0-20181028064349-e517b90714f7 // indirect
	github.com/sasha-s/go-deadlock v0.2.0 // indirect
	github.com/shopspring/decimal v0.0.0-20180709203117-cd690d0c9e24
	github.com/sirupsen/logrus v1.2.0
	github.com/spf13/viper v1.3.2
	github.com/tevino/abool v0.0.0-20170917061928-9b9efcf221b5 // indirect
	github.com/xdg/scram v0.0.0-20180814205039-7eeb5667e42c // indirect
	github.com/xdg/stringprep v1.0.0 // indirect
	go.mongodb.org/mongo-driver v1.0.1
	golang.org/x/crypto v0.0.0-20190418165655-df01cb2cc480
	gopkg.in/go-playground/validator.v8 v8.18.2 // indirect
	gopkg.in/tomb.v2 v2.0.0-20161208151619-d5d1b5820637 // indirect
)
